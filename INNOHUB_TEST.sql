-- *************** SqlDBM: PostgreSQL ****************;
-- ***************************************************;


-- ************************************** "Estados"

CREATE TABLE "Estados"
(
 "IdEstado" numeric NOT NULL,
 "Nombre"   text NOT NULL

);


-- ************************************** "Generos"

CREATE TABLE "Generos"
(
 "IdGenero" numeric NOT NULL,
 "Nombre"   text NOT NULL

);


-- ************************************** "Municipios"

CREATE TABLE "Municipios"
(
 "IdMunicipio" numeric NOT NULL,
 "Nombre"      text NOT NULL

);


-- ************************************** "Nacionalidades"

CREATE TABLE "Nacionalidades"
(
 "IdNacionalidad" numeric NOT NULL,
 "Nombre"         text NOT NULL

);


-- ************************************** "Paises"

CREATE TABLE "Paises"
(
 "IdPais" numeric NOT NULL,
 "Nombre" text NOT NULL

);



-- ************************************** "Usuarios"

CREATE TABLE "Usuarios"
(
 "IdUser"         numeric NOT NULL,
 "Nombre"         text NOT NULL,
 "Apellidos"      text NOT NULL,
 "CURP"           text NOT NULL,
 "RFC"            text NOT NULL,
 "Calle"          text NOT NULL,
 "Colonia"        text NOT NULL,
 "Username"       text NOT NULL,
 "Password"       text NOT NULL,
 "IdPais"         numeric NOT NULL,
 "IdEstado"       numeric NOT NULL,
 "IdMunicipio"    numeric NOT NULL,
 "IdGenero"       numeric NOT NULL,
 "IdNacionalidad" numeric NOT NULL,
 FOREIGN KEY ( "IdPais" ) REFERENCES "Paises" ( "IdPais" ),
 FOREIGN KEY ( "IdEstado" ) REFERENCES "Estados" ( "IdEstado" ),
 FOREIGN KEY ( "IdMunicipio" ) REFERENCES "Municipios" ( "IdMunicipio" ),
 FOREIGN KEY ( "IdGenero" ) REFERENCES "Generos" ( "IdGenero" ),
 FOREIGN KEY ( "IdNacionalidad" ) REFERENCES "Nacionalidades" ( "IdNacionalidad" )
);











