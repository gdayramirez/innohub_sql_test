
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ObjectId = Schema.ObjectId;

const Usuario = new Schema({
  usuarioid: ObjectId,
  nombre: { type: String },
  apellidos: { type: String },
  curp: { type: String },
  rfc: { type: String },
  direccion: [
    {
      calle: { type: String },
      municipio: { type: Number },
      estado: { type: Number },
      colonia: { type: String },
      pais: { type: Number }
    }
  ],
  sexo: { type: String },
  nacionalidad: { type: Number },
  username: { type: String },
  password: { type: String }
});


